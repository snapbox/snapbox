package com.example.snapbox;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TimePicker;

public class WeeklyTimeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(android.R.style.Theme_Holo_Dialog);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_weekly_time);
		
		final TimePicker startTimePicker = (TimePicker) findViewById(R.id.timePicker_weekly_start);
		final TimePicker endTimePicker = (TimePicker) findViewById(R.id.timePicker_weekly_end);
		
		final NumberPicker startDay = (NumberPicker) findViewById(R.id.numberPicker_weekly_start);
		final NumberPicker endDay = (NumberPicker) findViewById(R.id.numberPicker_weekly_end);
		
		final String[] days = {"MON", "TUES", "WED", "THUR", "FRI", "SAT", "SUN"}; 
		
		startDay.setMinValue(0);
		startDay.setMaxValue(6);
		endDay.setMinValue(0);
		endDay.setMaxValue(6);
		
		startDay.setDisplayedValues(days);
		startDay.setValue(0);
		endDay.setDisplayedValues(days);
		endDay.setValue(0);
		
		Button confirm = (Button) findViewById(R.id.button_weekly_confirm);
		Button delete = (Button) findViewById(R.id.button_weekly_delete);
		
		final String eventStr = getIntent().getExtras().getString("event_name");
		final String timeStr = getIntent().getExtras().getString("time");
		final Event event = Event.find(Event.class, "name=?", eventStr).get(0);
		
		if(timeStr.equals("")) delete.setEnabled(false);
		else{
			WeeklyEventTime time = WeeklyEventTime.findById(WeeklyEventTime.class, Long.parseLong(timeStr));
			startTimePicker.setCurrentHour(time.startHour);
			startTimePicker.setCurrentMinute(time.startMinute);
			endTimePicker.setCurrentHour(time.endHour);
			endTimePicker.setCurrentMinute(time.endMinute);
			
			String start = time.startDay.day;
			String end = time.endDay.day;
			
			if(start.equals("TUES")) startDay.setValue(1);
			if(end.equals("TUES")) endDay.setValue(1);
			
			if(start.equals("WED")) startDay.setValue(2);
			if(end.equals("WED")) endDay.setValue(2);
		
			if(start.equals("THUR")) startDay.setValue(3);
			if(end.equals("THUR")) endDay.setValue(3);
		
			if(start.equals("FRI")) startDay.setValue(4);
			if(end.equals("FRI")) endDay.setValue(4);
		
			if(start.equals("SAT")) startDay.setValue(5);
			if(end.equals("SAT")) endDay.setValue(5);
		
			if(start.equals("SUN")) startDay.setValue(6);
			if(end.equals("SUN")) endDay.setValue(6);
		}
		
		confirm.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				DayOfWeek start = DayOfWeek.find(DayOfWeek.class, "day=?", days[startDay.getValue()]).get(0);
				DayOfWeek end = DayOfWeek.find(DayOfWeek.class, "day=?", days[endDay.getValue()]).get(0);
				
				if(timeStr.equals("")){
					
					WeeklyEventTime time = new WeeklyEventTime(getBaseContext(), startTimePicker.getCurrentHour(), 
							startTimePicker.getCurrentMinute(), endTimePicker.getCurrentHour(), endTimePicker.getCurrentMinute(), 
							start, 
							end, event);
					time.save();
				}
				else {
					WeeklyEventTime time = WeeklyEventTime.findById(WeeklyEventTime.class, Long.parseLong(timeStr));
					time.startHour = startTimePicker.getCurrentHour();
					time.startMinute = startTimePicker.getCurrentMinute();
					time.endHour = endTimePicker.getCurrentHour();
					time.endMinute = endTimePicker.getCurrentMinute();
					time.startDay = start;
					time.endDay = end;
					time.save();
				}
				finish();
				//Intent myIntent = new Intent(v.getContext(),
				//		DailyTimeActivity.class);
				//startActivityForResult(myIntent, 0);
			}
		});
		
		delete.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				WeeklyEventTime.findById(WeeklyEventTime.class, Long.parseLong(timeStr)).delete();
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.weekly_time, menu);
		return true;
	}

}
