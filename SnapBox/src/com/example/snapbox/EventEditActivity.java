package com.example.snapbox;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class EventEditActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(android.R.style.Theme_Holo_Dialog);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_edit);
		
		//String name = null;
		
				final TextView name = (TextView) findViewById(R.id.text_event_name);
				final CheckBox active = (CheckBox) findViewById(R.id.check_active);
				final CheckBox custom = (CheckBox) findViewById(R.id.check_custom_file_names);
				final CheckBox byName = (CheckBox) findViewById(R.id.check_name);
				final CheckBox byTime = (CheckBox) findViewById(R.id.check_time);
				final CheckBox byDate = (CheckBox) findViewById(R.id.check_date);
				final CheckBox byCount = (CheckBox) findViewById(R.id.check_count);
				
				//byName.setEnabled(false);
				//byTime.setEnabled(false);
				//byDate.setEnabled(false);
				//byCount.setEnabled(false);
				
				Button confirm = (Button) findViewById(R.id.button_confirm_edits);
				Button delete = (Button) findViewById(R.id.button_delete_event);
				Button add = (Button) findViewById(R.id.button_new_timeframe);
				
				final String str = getIntent().getExtras().getString("event_name");
				
				name.setText(str);
				
				final Event event = Event.find(Event.class, "name=?", str).get(0);
				String event_name = event.getId().toString();
				final NamingScheme scheme = NamingScheme.find(NamingScheme.class, "event=?", event_name).get(0);
				final List<DailyEventTime> daily = DailyEventTime.find(DailyEventTime.class, "event=?", event_name);
				
				active.setChecked(event.isActive);
				custom.setChecked(event.hasCustomFilename);
				
				byName.setChecked(scheme.useName);
				byTime.setChecked(scheme.useTime);
				byDate.setChecked(scheme.useDate);
				byCount.setChecked(scheme.useCount);
				
				if(!custom.isChecked()){
					byName.setEnabled(false);
					byTime.setEnabled(false);
					byDate.setEnabled(false);
					byCount.setEnabled(false);	
				}
				
				
				
				///----> NOT IMPLEMENTED YET
				/*RadioButton delimiter0 = (RadioButton) findViewById(R.id.radio0);
				RadioButton delimiter1 = (RadioButton) findViewById(R.id.radio1);
				RadioButton delimiter2 = (RadioButton) findViewById(R.id.radio2);*/
				
				custom.setOnClickListener(new OnClickListener(){
					public void onClick(View v){
						if(((CheckBox) v).isChecked()){
							byName.setEnabled(true);
							byTime.setEnabled(true);
							byDate.setEnabled(true);
							byCount.setEnabled(true);
						}
						else{
							byName.setEnabled(false);
							byTime.setEnabled(false);
							byDate.setEnabled(false);
							byCount.setEnabled(false);
						}
					}
				});
				
				delete.setOnClickListener(new OnClickListener(){
					public void onClick(View v){
						for(DailyEventTime time: daily){
							time.delete();
						}
						scheme.delete();
						event.delete();
						finish();
						//Intent myIntent = new Intent(v.getContext(),
						//		EventEditorActivity.class);
						//startActivityForResult(myIntent, 0);
					}
				});
				
				confirm.setOnClickListener(new OnClickListener(){
					public void onClick(View v){
						
						event.isActive = active.isChecked();
						event.hasCustomFilename = custom.isChecked();
						event.save();
						
						scheme.useCount = byCount.isChecked();
						scheme.useDate = byDate.isChecked();
						scheme.useName = byName.isChecked();
						scheme.useTime = byTime.isChecked();
						scheme.save();
						
						finish();
						//Intent myIntent = new Intent(v.getContext(),
						//		EventEditorActivity.class);
						//startActivityForResult(myIntent, 0);
					}
				});
				
				add.setOnClickListener(new OnClickListener(){
					public void onClick(View v){
						Intent myIntent = new Intent(v.getContext(),
								TimesActivity.class);
						myIntent.putExtra("event_name", event.name);
						startActivityForResult(myIntent, 0);
					}
				});
				
				
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event_edit, menu);
		return true;
	}

}
