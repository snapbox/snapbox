package com.example.snapbox;

import android.content.Context;

import com.orm.SugarRecord;

public class Separator extends SugarRecord<Separator> {
	String separator;

	public Separator(Context context) {
		super(context);
	}

	public Separator(Context context, String separatorChar) {
		super(context);
		this.separator = separatorChar;
	}

}
