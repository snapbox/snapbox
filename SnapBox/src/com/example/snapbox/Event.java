package com.example.snapbox;

import android.content.Context;

import com.orm.SugarRecord;

public class Event extends SugarRecord<Event> {
	String name;
	boolean isActive;
	boolean hasCustomFilename;
	//RepeatType repeatType;

	public Event(Context context) {
		super(context);
	}

	public Event(Context context, String name, boolean isActive, boolean hasCustomFilename){ //, RepeatType repeatType) {
		super(context);
		this.name = name;
		this.isActive = isActive;
		this.hasCustomFilename = hasCustomFilename;
		//this.repeatType = repeatType;
	}
}
