package com.example.snapbox;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DBTestActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dbtest);

		final Button btn_dbadd = (Button) findViewById(R.id.btn_db_add);
		final Button btn_dbupdate = (Button) findViewById(R.id.btn_db_list);
		final TextView dblist = (TextView) findViewById(R.id.textView_dblist);

		final Context context = this;

		btn_dbadd.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				Separator sep = new Separator(context, "~");
//				sep.save();
//				RepeatType rt = new RepeatType(context, "weekly");
//				rt.save();
//				Event e = new Event(context, "lookup_test_event", true, false, rt);
//				e.save();
//				NamingScheme ns = new NamingScheme(context, true, false, false, false, e, sep);
//				ns.save();
			}
		});

		btn_dbupdate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				List<Event> events = Event.listAll(Event.class);
//				Event e = null;
//				for (Event ev: events) {
//					if (ev.name.equals("lookup_test_event")) {
//						dblist.append("\n\n Event Found! \n\n");
//						e = ev;
//					}
//				}
				// version 1
//				String eventID = e.getId().toString();
//				NamingScheme ns = NamingScheme.find(NamingScheme.class, "event = ?", eventID).get(0);

				// version 2
//				NamingScheme ns = NamingScheme.find(NamingScheme.class, "event = ?", e.getId().toString()).get(0);
				
				// version 3
//				NamingScheme ns = NamingScheme.find(NamingScheme.class, "event = ?", String.valueOf(e.getId())).get(0);
//				dblist.append("\n\n" + ns.toString());
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dbtest, menu);
		return true;
	}

}
