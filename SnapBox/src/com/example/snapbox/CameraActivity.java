package com.example.snapbox;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PictureCallback;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

public class CameraActivity extends Activity implements
		EventDialogFragmentListener {
	/*
	 * Lots of this code was taken or derived from
	 * http://developer.android.com/training/camera/photobasics.html and
	 * http://developer.android.com/guide/topics/media/camera.html
	 */

	static final int REQUEST_TAKE_PHOTO = 1;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	private Camera mCamera;
	private CameraPreview mPreview;

	List<Event> currentEvents;

	Context context = this; 

	List<Event> selectedEvents;

	String mCurrentPhotoPath;
	
	private File createImageFile(Event e) throws IOException {
		File image = null;

		if (e != null) {
			File storageDir = new File(SnapboxHelper.BASE_FILEPATH + e.name);
			if (!storageDir.exists()) {
				storageDir.mkdirs();
			}
			
//			Toast.makeText(this, SnapboxHelper.getNextFilename(e), Toast.LENGTH_LONG).show();
			
			File toBe = new File(storageDir, SnapboxHelper.getNextFilename(e)
					+ ".jpg");
			if (toBe.exists()) {
				image = File.createTempFile(SnapboxHelper.getNextFilename(e),
						".jpg", storageDir);
			} else {
				image = toBe;
			}
			mCurrentPhotoPath = "file: " + image.getAbsolutePath();
		}

		return image;
	}

	private PictureCallback mPicture = new PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			for (Event e : selectedEvents) {
				File pictureFile;
				try {
					pictureFile = createImageFile(e);
				} catch (IOException ex) {
					Log.d(this.toString(),
							"Error creating media file, check storage permissions: "
									+ ex.getMessage());
					return;
				}
				if (pictureFile == null) {
					Toast.makeText(context, "image file was null",
							Toast.LENGTH_LONG).show();
					Log.d(this.toString(),
							"Error creating media file, check storage permissions");
					return;
				}
				try {
					FileOutputStream fos = new FileOutputStream(pictureFile);
					fos.write(data);
					fos.close();
					Calendar cal = Calendar.getInstance();
					PictureTaken picTaken = new PictureTaken(context,
							SnapboxHelper.getYear(cal),
							SnapboxHelper.getMonth(cal),
							SnapboxHelper.getDay(cal),
							SnapboxHelper.getHour(cal),
							SnapboxHelper.getMinute(cal), e);
					picTaken.save();
					camera.startPreview();
				} catch (FileNotFoundException ex) {
					Log.d(this.toString(), "File not found: " + ex.getMessage());
				} catch (IOException ex) {
					Log.d(this.toString(),
							"Error accessing file: " + ex.getMessage());
				}
			}
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// if (requestCode == 1) { // set in the intent result thing above
		// if (resultCode == RESULT_OK) {
		// String result = data.getStringExtra("result");
		// }
		// if (resultCode == RESULT_CANCELED) {
		// // Write your code if there's no result
		// }
		// } else {
		addImageToGallery();
		// }
	}

	private void addImageToGallery() {
		Intent mediaScanIntent = new Intent(
				Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		File f = new File(mCurrentPhotoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		this.sendBroadcast(mediaScanIntent);
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera);

		try {
			// try to get the first one.
			// if it throws and exception it means there are none, so we need to create the defaults.
			@SuppressWarnings("unused")
			DayOfWeek day = DayOfWeek.listAll(DayOfWeek.class).get(0);
			@SuppressWarnings("unused")
			Separator sep = Separator.listAll(Separator.class).get(0);
		}
		catch(Exception e) {
			 DayOfWeek sunday = new DayOfWeek(this, "SUN");
			 sunday.save();
			 DayOfWeek monday = new DayOfWeek(this, "MON");
			 monday.save();
			 DayOfWeek tuesday = new DayOfWeek(this, "TUES");
			 tuesday.save();
			 DayOfWeek wednesday = new DayOfWeek(this, "WED");
			 wednesday.save();
			 DayOfWeek thursday = new DayOfWeek(this, "THUR");
			 thursday.save();
			 DayOfWeek friday = new DayOfWeek(this, "FRI");
			 friday.save();
			 DayOfWeek saturday = new DayOfWeek(this, "SAT");
			 saturday.save();

			Separator period = new Separator(this, "+");
			period.save();
			Separator hyphen = new Separator(this, "-");
			hyphen.save();
			Separator underscore = new Separator(this, "_");
			underscore.save();
		}

		// make sure the folder where we store images is there.
		File imagesFolder = new File(SnapboxHelper.BASE_FILEPATH);
		if (!imagesFolder.exists()) {
			imagesFolder.mkdirs();
		}
		
		currentEvents = SnapboxHelper.getCurrentEvents();

		final Button mSnapButton = (Button) findViewById(R.id.button_capture);

		// Create an instance of Camera
		mCamera = getCameraInstance();

		// Create our Preview view and set it as the content of our activity.
		mPreview = new CameraPreview(this, mCamera);
		FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
		preview.addView(mPreview);
		mSnapButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// get an image from the camera
				mCamera.takePicture(null, null, mPicture);
			}
		});

		if (currentEvents == null || currentEvents.isEmpty()) {
			Toast.makeText(
					context,
					"No active events were found. Please create an event with active time slots before taking pictures.",
					Toast.LENGTH_LONG).show();
			Intent goToCreateEvent = new Intent(this, EventEditorActivity.class);
			startActivity(goToCreateEvent);
			finish();
		} else if (currentEvents.size() == 1) {
			selectedEvents = currentEvents;
		} else { // multiple matching events
			showChooseEventDialog();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.camera, menu);
		return true;
	}

	@Override
	protected void onPause() {
		super.onPause();
		releaseCamera(); // release the camera immediately on pause event
	}

	@Override
	protected void onStop() {
		super.onPause();
		releaseCamera(); // release the camera immediately on pause event
	}

	@Override
	protected void onDestroy() {
		super.onPause();
		releaseCamera(); // release the camera immediately on pause event
	}

	private void releaseCamera() {
		if (mCamera != null) {
			mCamera.release(); // release the camera for other applications
			mCamera = null;
		}
	}

	public static Camera getCameraInstance() {
		Camera c = null;
		CameraInfo camInfo = new CameraInfo();
		try {
			c = Camera.open(camInfo.facing); // attempt to get a Camera instance
		} catch (Exception e) {
			// Camera is not available (in use or does not exist)
		}
		return c; // returns null if camera is unavailable
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		Intent changeActivityIntent;
		switch (item.getItemId()) {
		case R.id.menuFileBrowser:
			changeActivityIntent = new Intent(getApplicationContext(),
					FileViewActivity.class);
			startActivity(changeActivityIntent);
			finish();
			return true;
		case R.id.menuEvents:
			changeActivityIntent = new Intent(getApplicationContext(),
					EventEditorActivity.class);
			startActivity(changeActivityIntent);
			finish();
			return true;
		case R.id.menuPreferences:
			changeActivityIntent = new Intent(getApplicationContext(),
					PreferenceActivity.class);
			startActivity(changeActivityIntent);
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	void showChooseEventDialog() {
		DialogFragment cef = new ChooseEventFragment();
		cef.show(getFragmentManager(), "ChooseEventDialog");
	}

	void showNoEventDialog() {
		DialogFragment nepf = new NewEventPromptFragment();
		nepf.show(getFragmentManager(), "NoEventDialog");
	}

	@Override
	public void onReturnValue(List<Event> events) {
		selectedEvents = events;
	}
}
