package com.example.snapbox;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

public class DailyTimeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(android.R.style.Theme_Holo_Dialog);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_daily_time);
		
		final TimePicker startTimePicker = (TimePicker) findViewById(R.id.timePicker_start);
		final TimePicker endTimePicker = (TimePicker) findViewById(R.id.timePicker_end);
		
		Button confirm = (Button) findViewById(R.id.button_time_confirm);
		Button delete = (Button) findViewById(R.id.button_time_delete);
		
		final String eventStr = getIntent().getExtras().getString("event_name");
		final String timeStr = getIntent().getExtras().getString("time");
		final Event event = Event.find(Event.class, "name=?", eventStr).get(0);
		
		if(timeStr.equals("")) delete.setEnabled(false);
		else{
			DailyEventTime time = DailyEventTime.findById(DailyEventTime.class, Long.parseLong(timeStr));
			startTimePicker.setCurrentHour(time.startHour);
			startTimePicker.setCurrentMinute(time.startMinute);
			endTimePicker.setCurrentHour(time.endHour);
			endTimePicker.setCurrentMinute(time.endMinute);
		}
		
		confirm.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(timeStr.equals("")){
					DailyEventTime time = new DailyEventTime(getBaseContext(), startTimePicker.getCurrentHour(), 
							startTimePicker.getCurrentMinute(), endTimePicker.getCurrentHour(), endTimePicker.getCurrentMinute(), event);
					time.save();
				}
				else {
					DailyEventTime time = DailyEventTime.findById(DailyEventTime.class, Long.parseLong(timeStr));
					time.startHour = startTimePicker.getCurrentHour();
					time.startMinute = startTimePicker.getCurrentMinute();
					time.endHour = endTimePicker.getCurrentHour();
					time.endMinute = endTimePicker.getCurrentMinute();
					time.save();
				}
				finish();
				//Intent myIntent = new Intent(v.getContext(),
				//		DailyTimeActivity.class);
				//startActivityForResult(myIntent, 0);
			}
		});
		
		delete.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				DailyEventTime.findById(DailyEventTime.class, Long.parseLong(timeStr)).delete();
				finish();
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.daily_time, menu);
		return true;
	}

}
