package com.example.snapbox;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class PreferenceActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new PreferencesFragment())
                .commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.preference, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		Intent changeActivityIntent;
		switch (item.getItemId()) {
		case R.id.menuCamera:
			changeActivityIntent = new Intent(getApplicationContext(), CameraActivity.class);
	        startActivity(changeActivityIntent);
	        finish();
			return true;
		case R.id.menuFileBrowser:
			changeActivityIntent = new Intent(getApplicationContext(),
					FileViewActivity.class);
			startActivity(changeActivityIntent);
			finish();
			return true;
		case R.id.menuEvents:
			changeActivityIntent = new Intent(getApplicationContext(),
					EventEditorActivity.class);
			startActivity(changeActivityIntent);
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}