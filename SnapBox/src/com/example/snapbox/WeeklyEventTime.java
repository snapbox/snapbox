package com.example.snapbox;

import android.content.Context;

import com.orm.SugarRecord;

public class WeeklyEventTime extends SugarRecord<WeeklyEventTime> {
	DayOfWeek startDay;
	DayOfWeek endDay;
	int startHour;
	int startMinute;
	int endHour;
	int endMinute;
	Event event;

	public WeeklyEventTime(Context context) {
		super(context);
	}

	public WeeklyEventTime(Context context, int startHour, int startMinute,
			int endHour, int endMinute, DayOfWeek startDay, DayOfWeek endDay, Event event) {
		super(context);
		this.startDay = startDay;
		this.endDay = endDay;
		this.startHour = startHour;
		this.startMinute = startMinute;
		this.endHour = endHour;
		this.endMinute = endMinute;
		this.event = event;
	}

}