package com.example.snapbox;

import android.content.Context;

import com.orm.SugarRecord;

public class NamingScheme extends SugarRecord<NamingScheme> {
	boolean useName;
	boolean useDate;
	boolean useTime;
	boolean useCount;
	Event event;
	Separator separator;

	public NamingScheme(Context context) {
		super(context);
	}

	public NamingScheme(Context context, boolean useName, boolean useDate,
			boolean useTime, boolean useCount, Event event,
			Separator separator) {
		super(context);
		this.useName = useName;
		this.useDate = useDate;
		this.useTime = useTime;
		this.useCount = useCount;
		this.event = event;
		this.separator = separator;
	}

}
