package com.example.snapbox;

import android.content.Context;

import com.orm.SugarRecord;

public class DailyEventTime extends SugarRecord<DailyEventTime> {
	int startHour;
	int startMinute;
	int endHour;
	int endMinute;
	Event event;

	public DailyEventTime(Context context) {
		super(context);
	}

	public DailyEventTime(Context context, int startHour, int startMinute,
			int endHour, int endMinute, Event event) {
		super(context);
		this.startHour = startHour;
		this.startMinute = startMinute;
		this.endHour = endHour;
		this.endMinute = endMinute;
		this.event = event;
	}

}