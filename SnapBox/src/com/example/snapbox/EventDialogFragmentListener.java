package com.example.snapbox;

import java.util.List;

public interface EventDialogFragmentListener {
    public void onReturnValue(List<Event> event);
}