package com.example.snapbox;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		
		final TextView tv = (TextView) findViewById(R.id.tv_profile);
		List<Event> events = SnapboxHelper.getCurrentEvents();
		for (Event e : events) {
			tv.append(e.name + "\n");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.profile, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		Intent changeActivityIntent;
		switch (item.getItemId()) {
		case R.id.menuCamera:
			Toast.makeText(this, "Camera", Toast.LENGTH_SHORT).show();
			changeActivityIntent = new Intent(getApplicationContext(), CameraActivity.class);
	        startActivity(changeActivityIntent);
	        finish();
			return true;
		case R.id.menuFileBrowser:
			Toast.makeText(this, "File Browser", Toast.LENGTH_SHORT).show();
			changeActivityIntent = new Intent(getApplicationContext(), FileViewActivity.class);
	        startActivity(changeActivityIntent);
	        finish();
			return true;
		case R.id.menuEvents:
			Toast.makeText(this, "Events (not implemented yet)", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.menuPreferences:
			Toast.makeText(this, "Preferences", Toast.LENGTH_SHORT).show();
			changeActivityIntent = new Intent(getApplicationContext(), PreferenceActivity.class);
	        startActivity(changeActivityIntent);
	        finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}