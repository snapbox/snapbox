package com.example.snapbox;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.os.Environment;

public final class SnapboxHelper {
	// Suppress default constructor for noninstantiability
	private SnapboxHelper() {
		throw new AssertionError();
	}

	public static final int TIME_BUFFER = 10;
	public static final String BASE_FILEPATH = Environment
			.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
			+ "/SnapBox/";
	public static final String[] DAYS_OF_WEEK = { "SUN", "MON", "TUES", "WED",
			"THUR", "FRI", "SAT" };

	public static final SimpleDateFormat yearFormat = new SimpleDateFormat(
			"yyyy", Locale.US);
	public static final SimpleDateFormat monthOfYearFormat = new SimpleDateFormat(
			"M", Locale.US);
	public static final SimpleDateFormat dayOfMonthFormat = new SimpleDateFormat(
			"d", Locale.US);
	public static final SimpleDateFormat dayOfWeekFormat = new SimpleDateFormat(
			"EEEE", Locale.US);
	public static final SimpleDateFormat hourOfDayFormat = new SimpleDateFormat(
			"H", Locale.US);
	public static final SimpleDateFormat minuteOfHourFormat = new SimpleDateFormat(
			"m", Locale.US);

	public static int getUnixTime() {
		return (int) (System.currentTimeMillis() / 1000L);
	}

	public static String getTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm:ss", Locale.getDefault());
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}

	public static int getYear(Calendar cal) {
		return Integer.valueOf(yearFormat.format(cal.getTime()));
	}

	public static int getMonth(Calendar cal) {
		return Integer.valueOf(monthOfYearFormat.format(cal.getTime()));
	}

	public static int getDay(Calendar cal) {
		return Integer.valueOf(dayOfMonthFormat.format(cal.getTime()));
	}

	public static int getHour(Calendar cal) {
		return Integer.valueOf(hourOfDayFormat.format(cal.getTime()));
	}

	public static int getMinute(Calendar cal) {
		return Integer.valueOf(minuteOfHourFormat.format(cal.getTime()));
	}

	// public static boolean isCurrent(Event e) {
	//
	// }

	// public static int getFullTime(Event e) {
	//
	// }

	// public static int getFullTime(Calendar cal) {
	// Date d = cal.getTime();
	// String year = yearFormat.format(d);
	// String month = monthOfYearFormat.format(d);
	// String day = dayOfMonthFormat.format(d);
	// String hour = hourOfDayFormat.format(d);
	// String minute = minuteOfHourFormat.format(d);
	// }

	public static List<Event> getCurrentEvents() {

		Calendar cal = Calendar.getInstance();
		Date today = cal.getTime();
		int year = Integer.parseInt(SnapboxHelper.yearFormat.format(today));
		int month = Integer.parseInt(SnapboxHelper.monthOfYearFormat
				.format(today));
		int dayOfMonth = Integer.parseInt(SnapboxHelper.dayOfMonthFormat
				.format(today));
		// String dayOfWeekName = SnapboxHelper.dayOfWeekFormat
		// .format(today);
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		int hour = Integer
				.parseInt(SnapboxHelper.hourOfDayFormat.format(today));
		int minute = Integer.parseInt(SnapboxHelper.minuteOfHourFormat
				.format(today));

		List<Event> events = new ArrayList<Event>();

		// find matching time slots and days and add them to the list

		// the checks for these are wayyyyyyy too specific. fix ranges.

		// non-repeating events
		for (NonRepeatingEventTime nret : NonRepeatingEventTime
				.listAll(NonRepeatingEventTime.class)) {
			Event e = nret.event;
			if (year > nret.startYear && year < nret.endYear) {
				if (e.isActive) {
					events.add(e);
				}
			} else if (year == nret.startYear || year == nret.endYear) {
				if (month > nret.startMonth && month < nret.endMonth) {
					if (e.isActive) {
						events.add(e);
					}
				} else if (month == nret.startMonth || month == nret.endMonth) {
					if (dayOfMonth > nret.startDay && dayOfMonth < nret.endDay) {
						if (e.isActive) {
							events.add(e);
						}
					} else if (dayOfMonth == nret.startDay
							|| dayOfMonth == nret.endDay) {
						if (hour > nret.startHour && hour < nret.endHour) {
							if (e.isActive) {
								events.add(e);
							}
						} else if (hour == nret.startHour
								|| hour == nret.endHour) {
							if (minute >= nret.startMinute
									- SnapboxHelper.TIME_BUFFER
									&& minute <= nret.endMinute
											+ SnapboxHelper.TIME_BUFFER) {
								if (e.isActive) {
									events.add(e);
								}
							}
						}
					}
				}
			}
		}

		// daily events
		for (DailyEventTime det : DailyEventTime.listAll(DailyEventTime.class)) {
			Event e = det.event;
			if (hour > det.startHour && hour < det.endHour) {
				if (e.isActive) {
					events.add(e);
				}
			} else if (hour == det.startHour || hour == det.endHour) {
				if (minute >= det.startMinute - SnapboxHelper.TIME_BUFFER
						&& minute <= det.endMinute + SnapboxHelper.TIME_BUFFER) {
					if (e.isActive) {
						events.add(e);
					}
				}
			}
		}

		// weekly events
		for (WeeklyEventTime wet : WeeklyEventTime
				.listAll(WeeklyEventTime.class)) {
			Event e = wet.event;
			if (dayOfWeek > getIntFromDayName(wet.startDay.day)
					&& dayOfWeek > getIntFromDayName(wet.endDay.day)) {
				if (e.isActive) {
					events.add(e);
				}
			} else if (dayOfWeek == getIntFromDayName(wet.startDay.day)
					|| dayOfWeek == getIntFromDayName(wet.endDay.day)) {
				if (hour > wet.startHour && hour < wet.endHour) {
					if (e.isActive) {
						events.add(e);
					}
				} else if (hour == wet.startHour || hour == wet.endHour) {
					if (minute >= wet.startMinute - SnapboxHelper.TIME_BUFFER
							&& minute <= wet.endMinute
									+ SnapboxHelper.TIME_BUFFER) {
						if (e.isActive) {
							events.add(e);
						}
					}
				}
			}
		}

		return events;
	}

	private static int getIntFromDayName(String day) {
		return Arrays.asList(DAYS_OF_WEEK).indexOf(day);

	}

	public static String getNextFilename(Event e) {
		String imageFileName;
		Calendar cal = Calendar.getInstance();
		StringBuilder sb = new StringBuilder();
		if (e.hasCustomFilename) {
			// build from Naming_Scheme table
			String eventID = e.getId().toString();
			List<NamingScheme> ns = NamingScheme.find(NamingScheme.class,
					"event = ?", eventID);
			if (ns.size() != 1) {
				// TODO this shouldn't ever happen. prompt for selection?
			} else {
				NamingScheme scheme = ns.get(0);
				if (scheme.useName) {
					sb.append(e.name);
				}
				if (scheme.useDate) {
					sb.append(scheme.separator.separator);
					sb.append(getMonth(cal));
					sb.append(getDay(cal));
				}
				if (scheme.useTime) {
					sb.append(scheme.separator.separator);
					sb.append(getHour(cal));
					sb.append(getMinute(cal));
				}
				if (scheme.useCount) {
					List<EventPictureCount> epc = EventPictureCount.find(
							EventPictureCount.class, "event = ?", eventID);
					if (epc.size() != 1) {
						// TODO again, shouldn't happen. throw some sort of
						// exception
					} else {
						EventPictureCount imageCount = epc.get(0);
						imageCount.count += 1;
						int count = imageCount.count;
						imageCount.save();
						sb.append(scheme.separator.separator);
						sb.append(count);
					}
				}
				return sb.toString();
			}
			return "SomethingBadHappenedButIHaveToReturnSomethingSoThisIsItForNow.";
		} else {
			// generate default filename type
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
					Locale.US).format(new Date());
			imageFileName = e.name + "_" + timeStamp + "_";
		}
		return imageFileName;
	}
}
