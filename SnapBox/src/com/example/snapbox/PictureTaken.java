package com.example.snapbox;

import android.content.Context;

import com.orm.SugarRecord;

public class PictureTaken extends SugarRecord<PictureTaken> {
	int year;
	int month;
	int day;
	int hour;
	int minute;
	Event event;

	public PictureTaken(Context context) {
		super(context);
	}

	public PictureTaken(Context context, int year, int month, int day, int hour, int minute, Event event) {
		super(context);
		this.year = year;
		this.month = month;
		this.day = day;
		this.hour = hour;
		this.minute = minute;
		this.event = event;
	}

}
