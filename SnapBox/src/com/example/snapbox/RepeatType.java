package com.example.snapbox;

import android.content.Context;

import com.orm.SugarRecord;

public class RepeatType extends SugarRecord<RepeatType> {
	
	RepeatFrequency frequency;
	
	public RepeatType(Context context) {
		super(context);
	}

	public RepeatType(Context context, RepeatFrequency frequency) {
		super(context);
		this.frequency = frequency;
	}
}
