package com.example.snapbox;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;

public class OnceTimeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(android.R.style.Theme_Holo_Dialog);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_once_time);
		
		final TimePicker startTimePicker = (TimePicker) findViewById(R.id.timePicker_once_start);
		final TimePicker endTimePicker = (TimePicker) findViewById(R.id.timePicker_once_end);
		
		Button confirm = (Button) findViewById(R.id.button_once_confirm);
		Button delete = (Button) findViewById(R.id.button_once_delete);
		
		final DatePicker startDatePicker = (DatePicker) findViewById(R.id.datePicker_once_start);
		final DatePicker endDatePicker = (DatePicker) findViewById(R.id.datePicker_once_end);
		
		final String eventStr = getIntent().getExtras().getString("event_name");
		final String timeStr = getIntent().getExtras().getString("time");
		final Event event = Event.find(Event.class, "name=?", eventStr).get(0);
		
		if(timeStr.equals("")) delete.setEnabled(false);
		else{
			NonRepeatingEventTime time = NonRepeatingEventTime.findById(NonRepeatingEventTime.class, Long.parseLong(timeStr));
			startTimePicker.setCurrentHour(time.startHour);
			startTimePicker.setCurrentMinute(time.startMinute);
			endTimePicker.setCurrentHour(time.endHour);
			endTimePicker.setCurrentMinute(time.endMinute);
			
			startDatePicker.updateDate(time.startYear, time.startMonth, time.startDay);
			endDatePicker.updateDate(time.endYear, time.endMonth, time.endDay);
		}
		
		confirm.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(timeStr.equals("")){
					NonRepeatingEventTime time = new NonRepeatingEventTime(getBaseContext(), startDatePicker.getYear(), 
							startDatePicker.getMonth() + 1, startDatePicker.getDayOfMonth(), startTimePicker.getCurrentHour(),
							startTimePicker.getCurrentMinute(), endDatePicker.getYear(), 
							endDatePicker.getMonth(), endDatePicker.getDayOfMonth(), endTimePicker.getCurrentHour(),
							endTimePicker.getCurrentMinute(), event);
					time.save();
				}
				else {
					NonRepeatingEventTime time = NonRepeatingEventTime.findById(NonRepeatingEventTime.class, Long.parseLong(timeStr));
					time.startYear = startDatePicker.getYear();
					time.startMonth = startDatePicker.getMonth() + 1;
					time.startDay = startDatePicker.getDayOfMonth();
					time.startHour = startTimePicker.getCurrentHour();
					time.startMinute = startTimePicker.getCurrentMinute();
					time.endYear = endDatePicker.getYear();
					time.endMonth = endDatePicker.getMonth();
					time.endDay = endDatePicker.getDayOfMonth();
					time.endHour = endTimePicker.getCurrentHour();
					time.endMinute = endTimePicker.getCurrentMinute();
					time.save();
				}
				finish();
				//Intent myIntent = new Intent(v.getContext(),
				//		DailyTimeActivity.class);
				//startActivityForResult(myIntent, 0);
			}
		});
		
		delete.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				NonRepeatingEventTime.findById(NonRepeatingEventTime.class, Long.parseLong(timeStr)).delete();
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.once_time, menu);
		return true;
	}

}
