package com.example.snapbox;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		try {
			// try to get the first one.
			// if it throws and exception it means there are none, so we need to create the defaults.
			@SuppressWarnings("unused")
			DayOfWeek day = DayOfWeek.listAll(DayOfWeek.class).get(0);
			@SuppressWarnings("unused")
			Separator sep = Separator.listAll(Separator.class).get(0);
		}
		catch(Exception e) {
			 DayOfWeek sunday = new DayOfWeek(this, "SUN");
			 sunday.save();
			 DayOfWeek monday = new DayOfWeek(this, "MON");
			 monday.save();
			 DayOfWeek tuesday = new DayOfWeek(this, "TUES");
			 tuesday.save();
			 DayOfWeek wednesday = new DayOfWeek(this, "WED");
			 wednesday.save();
			 DayOfWeek thursday = new DayOfWeek(this, "THUR");
			 thursday.save();
			 DayOfWeek friday = new DayOfWeek(this, "FRI");
			 friday.save();
			 DayOfWeek saturday = new DayOfWeek(this, "SAT");
			 saturday.save();

			Separator period = new Separator(this, "+");
			period.save();
			Separator hyphen = new Separator(this, "-");
			hyphen.save();
			Separator underscore = new Separator(this, "_");
			underscore.save();
		}

		// make sure the folder where we store images is there.
		File imagesFolder = new File(SnapboxHelper.BASE_FILEPATH);
		if (!imagesFolder.exists()) {
			imagesFolder.mkdirs();
		}

		final Button buttonProfile = (Button) findViewById(R.id.btn_db_add);
		final Button buttonSchedule = (Button) findViewById(R.id.button_new_daily);
		final Button buttonPreferences = (Button) findViewById(R.id.button3);
		final Button buttonFileViewer = (Button) findViewById(R.id.button4);
		final Button btn_dbtest = (Button) findViewById(R.id.button5);
		final Button btn_go2camera = (Button) findViewById(R.id.button_go2cam);

		buttonProfile.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),
						ProfileActivity.class);
				startActivityForResult(myIntent, 0);
			}
		});

		buttonSchedule.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),
						ScheduleActivity.class);
				startActivityForResult(myIntent, 0);
			}
		});

		buttonPreferences.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),
						PreferenceActivity.class);
				startActivityForResult(myIntent, 0);
			}
		});

		buttonFileViewer.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),
						FileViewActivity.class);
				startActivityForResult(myIntent, 0);
			}
		});

		btn_dbtest.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),
						EventEditorActivity.class);
				startActivityForResult(myIntent, 0);
			}
		});

		btn_go2camera.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),
						CameraActivity.class);
				startActivityForResult(myIntent, 0);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		Intent changeActivityIntent;
		switch (item.getItemId()) {
		case R.id.menuCamera:
			Toast.makeText(this, "Camera", Toast.LENGTH_SHORT).show();
			changeActivityIntent = new Intent(getApplicationContext(), CameraActivity.class);
	        startActivity(changeActivityIntent);
	        finish();
			return true;
		case R.id.menuFileBrowser:
			Toast.makeText(this, "File Browser", Toast.LENGTH_SHORT).show();
			changeActivityIntent = new Intent(getApplicationContext(), FileViewActivity.class);
	        startActivity(changeActivityIntent);
	        finish();
			return true;
		case R.id.menuEvents:
			Toast.makeText(this, "Events (not implemented yet)", Toast.LENGTH_SHORT).show();
			return true;
		case R.id.menuPreferences:
			Toast.makeText(this, "Preferences", Toast.LENGTH_SHORT).show();
			changeActivityIntent = new Intent(getApplicationContext(), PreferenceActivity.class);
	        startActivity(changeActivityIntent);
	        finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}