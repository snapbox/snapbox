package com.example.snapbox;

import android.content.Context;

import com.orm.SugarRecord;

public class RepeatFrequency extends SugarRecord<RepeatFrequency> {
	
	String frequency;
	
	public RepeatFrequency(Context context) {
		super(context);
	}

	public RepeatFrequency(Context context, String frequency) {
		super(context);
		this.frequency = frequency;
	}
}
