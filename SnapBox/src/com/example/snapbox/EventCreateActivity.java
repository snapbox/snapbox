package com.example.snapbox;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class EventCreateActivity extends Activity {

	public int radio = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(android.R.style.Theme_Holo_Dialog);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_create);
		
		
		final Context context = this;
		//String name = null;
		
		final EditText nameField = (EditText) findViewById(R.id.event_name);
		final CheckBox active = (CheckBox) findViewById(R.id.check_active);
		final CheckBox custom = (CheckBox) findViewById(R.id.check_custom_file_names);
		final CheckBox byName = (CheckBox) findViewById(R.id.check_name);
		final CheckBox byTime = (CheckBox) findViewById(R.id.check_time);
		final CheckBox byDate = (CheckBox) findViewById(R.id.check_date);
		final CheckBox byCount = (CheckBox) findViewById(R.id.check_count);
		
		byName.setEnabled(false);
		byTime.setEnabled(false);
		byDate.setEnabled(false);
		byCount.setEnabled(false);
		
		active.setChecked(true);  // active by default
		
		Button create = (Button) findViewById(R.id.button_new_timeframe);
		
		
		///----> NOT IMPLEMENTED YET
		/*RadioButton delimiter0 = (RadioButton) findViewById(R.id.radio0);
		RadioButton delimiter1 = (RadioButton) findViewById(R.id.radio1);
		RadioButton delimiter2 = (RadioButton) findViewById(R.id.radio2);*/
		
		custom.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				if(((CheckBox) v).isChecked()){
					byName.setEnabled(true);
					byTime.setEnabled(true);
					byDate.setEnabled(true);
					byCount.setEnabled(true);
				}
				else{
					byName.setEnabled(false);
					byTime.setEnabled(false);
					byDate.setEnabled(false);
					byCount.setEnabled(false);
				}
			}
		});
		
		create.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				boolean fine = true;
				String name = nameField.getText().toString();
				List<Event> events = Event.listAll(Event.class);
				for(Event event: events){
					if(event.name.equals(name)) fine = false;
				}
				if(!fine){
					Toast.makeText(getApplicationContext(), "Event name Taken, please pick another one",
							   Toast.LENGTH_LONG).show();
				}
				else if (name.equals("")){
					Toast.makeText(getApplicationContext(), "Please enter an event name",
							   Toast.LENGTH_LONG).show();
				}
				else{
					Separator separator = null;
					if(radio == 0) separator = Separator.find(Separator.class, "separator=?", "+").get(0);
					if(radio == 1) separator = Separator.find(Separator.class, "separator=?", "-").get(0);
					if(radio == 2) separator = Separator.find(Separator.class, "separator=?", "_").get(0);
					
					Event event = new Event(context, name, active.isChecked(), custom.isChecked());
					event.save();
					
					if(custom.isChecked()){
						NamingScheme scheme = new NamingScheme(context, byName.isChecked(), byTime.isChecked(), 
								byDate.isChecked(), byCount.isChecked(), event, separator);
						scheme.save();
					}
					else{
						NamingScheme scheme = new NamingScheme(context, false, false, 
								false, false, event, separator);
						scheme.save();
					}
					finish();
					//Intent myIntent = new Intent(v.getContext(),
					//		EventEditorActivity.class);
					//startActivityForResult(myIntent, 0);
				}
			}
		});
	}

	public void onRadioButtonClicked(View view) {
	    // Is the button now checked?
	    boolean checked = ((RadioButton) view).isChecked();
	    
	    // Check which radio button was clicked
	    switch(view.getId()) {
	        case R.id.radio0:
	            if (checked)
	            	radio = 0;
	            break;
	        case R.id.radio1:
	            if (checked)
	            	radio = 1;
	            break;
	        case R.id.radio2:
	        	if (checked)
	        		radio = 2;
	        	break;
	    }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event_create, menu);
		return true;
	}

}
