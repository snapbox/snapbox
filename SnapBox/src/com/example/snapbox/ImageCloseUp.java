package com.example.snapbox;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

public class ImageCloseUp extends Activity {

	private String filepath;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_close_up);
		
		Bundle extras = getIntent().getExtras();
		filepath = extras.getString("pic_name");
		
		
		ImageView image = new ImageView(getApplicationContext());
		Bitmap pic = BitmapFactory.decodeFile(filepath);
		if (pic == null) {
			Toast.makeText(getApplicationContext(), "pic is null!", Toast.LENGTH_SHORT).show();
		}
		image.setImageBitmap(pic);
		image.setId(1);
		
		LinearLayout ImageLayout = (LinearLayout) findViewById(R.id.imageLayout);
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT); 
		ImageLayout.addView(image, params);
		
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.image_close_up, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
