package com.example.snapbox;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class TimesActivity extends Activity {
	
	private static String item;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_times);
		
		final TextView text = (TextView) findViewById(R.id.time_name);
		
		final ListView onceListview = (ListView) findViewById(R.id.listView_once_times);
		final ListView weeklyListview = (ListView) findViewById(R.id.listView_weekly_times);
		final ListView dailyListview = (ListView) findViewById(R.id.listView_daily_times);
			
		final Button newOnceButton = (Button) findViewById(R.id.button_new_once);
		final Button newWeeklyButton = (Button) findViewById(R.id.button_new_weekly);
		final Button newDailyButton = (Button) findViewById(R.id.button_new_daily);
		final Button confirmButton = (Button) findViewById(R.id.button_times_confirm);
		
		final String str = getIntent().getExtras().getString("event_name");
		
		text.setText("Times for " + str);
		
		final Event event = Event.find(Event.class, "name=?", str).get(0);
		String event_name = event.getId().toString();
		
		//final Button editButton = (Button) findViewById(R.id.button_edit);
		//final Button deleteButton = (Button) findViewById(R.id.button_del);
		
		List<DailyEventTime> daily = DailyEventTime.find(DailyEventTime.class, "event=?", event_name);
		List<WeeklyEventTime> weekly = WeeklyEventTime.find(WeeklyEventTime.class, "event=?", event_name);
		List<NonRepeatingEventTime> once = NonRepeatingEventTime.find(NonRepeatingEventTime.class, "event=?", event_name);
		
		final List<String> dailyIds = new ArrayList<String>();
		final List<String> weeklyIds = new ArrayList<String>();
		final List<String> onceIds = new ArrayList<String>();
		
		//editButton.setEnabled(false);
		//deleteButton.setEnabled(false);
		
		newOnceButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),
						OnceTimeActivity.class);
				myIntent.putExtra("event_name", event.name);
				myIntent.putExtra("time", "");
				startActivityForResult(myIntent, 0);
			}
		});
		
		newWeeklyButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),
						WeeklyTimeActivity.class);
				myIntent.putExtra("event_name", event.name);
				myIntent.putExtra("time", "");
				startActivityForResult(myIntent, 0);
			}
		});
		
		newDailyButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),
						DailyTimeActivity.class);
				myIntent.putExtra("event_name", event.name);
				myIntent.putExtra("time", "");
				startActivityForResult(myIntent, 0);
			}
		});
		
		confirmButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
		/*editButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),
						EventEditActivity.class);
				startActivityForResult(myIntent, 0);
			}
		});
		
		deleteButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				List<Event> events = Event.find(Event.class, "name=?", item);
				for(Event event: events){
					event.delete();
				}
				listview.invalidate();
			}
		});*/
		
		List<String> onceList = new ArrayList<String>();
		List<String> weeklyList = new ArrayList<String>();
		List<String> dailyList = new ArrayList<String>();
		
		for(NonRepeatingEventTime time: once){
			onceIds.add(time.getId().toString());
			onceList.add(time.startYear+"/"+time.startMonth+"/"+time.startDay+" "+String.format("%2d:%02d", time.startHour, time.startMinute)+
					" - "+time.endYear+"/"+time.endMonth+"/"+time.endDay+" "+String.format("%2d:%02d", time.endHour, time.endMinute));
		}
		for(WeeklyEventTime time: weekly){
			if(time.startDay != null){
			weeklyIds.add(time.getId().toString());
			weeklyList.add(time.startDay.day+" "+String.format("%2d:%02d", time.startHour, time.startMinute)+" - "+
					time.endDay.day+" "+String.format("%2d:%02d", time.endHour, time.endMinute));
			}
		}
		for(DailyEventTime time: daily){
			dailyIds.add(time.getId().toString());
			dailyList.add(String.format("%2d:%02d - %2d:%02d", time.startHour, time.startMinute, time.endHour, time.endMinute));
		}
		
		final ArrayAdapter<String> onceAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, onceList);
		onceListview.setAdapter(onceAdapter);
		
		onceListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

		      @Override
		      public void onItemClick(AdapterView<?> parent, final View view,
		          int position, long id) {
		        //item = (String) parent.getItemAtPosition(position);
		        
		        Intent myIntent = new Intent(view.getContext(),
						OnceTimeActivity.class);
		        myIntent.putExtra("event_name", event.name);
		        myIntent.putExtra("time", onceIds.get(position));
				startActivityForResult(myIntent, 0);
				
		        //editButton.setEnabled(true);
		        //deleteButton.setEnabled(true);
		      }

		});
		
		final ArrayAdapter<String> weeklyAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, weeklyList);
		weeklyListview.setAdapter(weeklyAdapter);
		
		weeklyListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

		      @Override
		      public void onItemClick(AdapterView<?> parent, final View view,
		          int position, long id) {
		        //item = (String) parent.getItemAtPosition(position);
		        
		        Intent myIntent = new Intent(view.getContext(),
						WeeklyTimeActivity.class);
		        myIntent.putExtra("event_name", event.name);
		        myIntent.putExtra("time", weeklyIds.get(position));
				startActivityForResult(myIntent, 0);
				
		        //editButton.setEnabled(true);
		        //deleteButton.setEnabled(true);
		      }

		});
		
		final ArrayAdapter<String> dailyAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dailyList);
		dailyListview.setAdapter(dailyAdapter);
		
		dailyListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

		      @Override
		      public void onItemClick(AdapterView<?> parent, final View view,
		          int position, long id) {
		        
		        Intent myIntent = new Intent(view.getContext(),
						DailyTimeActivity.class);
		        myIntent.putExtra("event_name", event.name);
		        myIntent.putExtra("time", dailyIds.get(position));
				startActivityForResult(myIntent, 0);
				
		        //editButton.setEnabled(true);
		        //deleteButton.setEnabled(true);
		      }

		});
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event_editor, menu);
		return true;
	}
	
	@Override
	protected void onResume() {

	   super.onResume();
	   this.onCreate(null);
	}


}
