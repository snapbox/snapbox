package com.example.snapbox;

import android.content.Context;

import com.orm.SugarRecord;

public class DayOfWeek extends SugarRecord<DayOfWeek> {
	String day;

	public DayOfWeek(Context context) {
		super(context);
	}

	public DayOfWeek(Context context, String day) {
		super(context);
		this.day = day;
	}
}
