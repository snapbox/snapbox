package com.example.snapbox;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class EventEditorActivity extends Activity {
	
	private static String item;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_editor);
		
		final ListView listview = (ListView) findViewById(R.id.listView_once_times);
		List<Event> events = Event.listAll(Event.class);
			
		final Button newButton = (Button) findViewById(R.id.button6);
		
		//final Button editButton = (Button) findViewById(R.id.button_edit);
		//final Button deleteButton = (Button) findViewById(R.id.button_del);
		
		//editButton.setEnabled(false);
		//deleteButton.setEnabled(false);
		
		newButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),
						EventCreateActivity.class);
				startActivityForResult(myIntent, 0);
			}
		});
		
		/*editButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),
						EventEditActivity.class);
				startActivityForResult(myIntent, 0);
			}
		});
		
		deleteButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				List<Event> events = Event.find(Event.class, "name=?", item);
				for(Event event: events){
					event.delete();
				}
				listview.invalidate();
			}
		});*/
		
		List<String> list = new ArrayList<String>();
		
		for(Event event: events){
			list.add(event.name);
		}
		
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
		listview.setAdapter(adapter);
		
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

		      @Override
		      public void onItemClick(AdapterView<?> parent, final View view,
		          int position, long id) {
		        item = (String) parent.getItemAtPosition(position);
		        
		        Intent myIntent = new Intent(view.getContext(),
						EventEditActivity.class);
		        myIntent.putExtra("event_name", item);
				startActivityForResult(myIntent, 0);
				
		        //editButton.setEnabled(true);
		        //deleteButton.setEnabled(true);
		      }

		});
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event_editor, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		Intent changeActivityIntent;
		switch (item.getItemId()) {
		case R.id.menuCamera:
			changeActivityIntent = new Intent(getApplicationContext(), CameraActivity.class);
	        startActivity(changeActivityIntent);
	        finish();
			return true;
		case R.id.menuFileBrowser:
			changeActivityIntent = new Intent(getApplicationContext(),
					FileViewActivity.class);
			startActivity(changeActivityIntent);
			finish();
			return true;
		case R.id.menuPreferences:
			changeActivityIntent = new Intent(getApplicationContext(),
					PreferenceActivity.class);
			startActivity(changeActivityIntent);
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void onResume() {

	   super.onResume();
	   this.onCreate(null);
	}

}
