package com.example.snapbox;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class FileViewActivity extends Activity {

	String filepath = SnapboxHelper.BASE_FILEPATH;
	private ListAdapter adapter;
	private Item[] fileList;
	private File path = new File(filepath);
	final Context ctx = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_view);

		GridView gridview = (GridView) findViewById(R.id.gridview);
		loadFileList();
		gridview.setAdapter(adapter);

		gridview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, final View v,
					int position, long id) {
				Bundle blunder = new Bundle();
				Intent newIntent = new Intent(ctx, ImgBrowser.class);
				String eventPath = filepath
						+ adapter.getItem(position).toString() + "";
				blunder.putString("event_name", eventPath);
				newIntent.putExtras(blunder);
				startActivity(newIntent);
			}
		});

	}

	private void loadFileList() {
		//path.mkdirs();

		if (path.exists()) {
			String[] fList = path.list();
			fileList = new Item[fList.length];
			for (int i = 0; i < fList.length; i++) {
				fileList[i] = new Item(fList[i],
						android.R.drawable.ic_dialog_alert);
				File select = new File(path, fList[i]);
				if (select.isDirectory())
					fileList[i].iconName = android.R.drawable.ic_menu_gallery;
			}

		}

		adapter = new ArrayAdapter<Item>(this,
				android.R.layout.select_dialog_item, android.R.id.text1,
				fileList) {

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View view = super.getView(position, convertView, parent);
				TextView textView = (TextView) view
						.findViewById(android.R.id.text1);

				// put the image on the TextView
				textView.setCompoundDrawablesWithIntrinsicBounds(0,
						fileList[position].iconName, 0, 0);

				// add margin between image and text
				int margin = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
				textView.setCompoundDrawablePadding(margin);

				return view;
			}
		};

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.file_view, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		Intent changeActivityIntent;
		switch (item.getItemId()) {
		case R.id.menuCamera:
			changeActivityIntent = new Intent(getApplicationContext(), CameraActivity.class);
	        startActivity(changeActivityIntent);
	        finish();
			return true;
		case R.id.menuEvents:
			changeActivityIntent = new Intent(getApplicationContext(),
					EventEditorActivity.class);
			startActivity(changeActivityIntent);
			finish();
			return true;
		case R.id.menuPreferences:
			changeActivityIntent = new Intent(getApplicationContext(),
					PreferenceActivity.class);
			startActivity(changeActivityIntent);
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private class Item {
		public String fname;
		public int iconName;

		public Item(String filename, int iname) {
			fname = filename;
			iconName = iname;
		}

		@Override
		public String toString() {
			return fname;
		}
	}

}
