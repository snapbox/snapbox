package com.example.snapbox;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

public class ImgBrowser extends Activity {
	
	private String filepath;
	//private ListAdapter adapter;
	private String[] fList;
	private File path;
	private Context mContext;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_img_browser);
		
		Bundle extras = getIntent().getExtras();
		filepath = extras.getString("event_name");
		path = new File(filepath);
		
		mContext = getApplicationContext();
		
		loadFileList();

		GridView gridview = (GridView) findViewById(R.id.gridview);
	    gridview.setAdapter(new ImageAdapter(this));

	    /*gridview.setOnItemClickListener(new OnItemClickListener() {
	        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
	            Toast.makeText(mContext, "Item clicked!", Toast.LENGTH_SHORT).show();
	        }
	    });*/

		
		gridview.setOnItemClickListener(new OnItemClickListener() {
	        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
	            Intent newIntent = new Intent(mContext, ImageCloseUp.class);
	            String fullpath = filepath + "/" + fList[position];
	            
	            Toast.makeText(mContext, fList[position], Toast.LENGTH_SHORT).show();
	            Bundle b = new Bundle();
	            b.putString("pic_name", fullpath);
	            newIntent.putExtras(b);
	            startActivity(newIntent);
	        }
	    });
	}
	
	private void loadFileList() {
		if (!path.exists()) {
			path.mkdirs();
		}

		fList = path.list();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.img_browser, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public class ImageAdapter extends BaseAdapter {
	    private Context mContext;

	    public ImageAdapter(Context c) {
	        mContext = c;
	    }

	    public int getCount() {
	        return fList.length;
	    }

	    public Object getItem(int position) {
	        return null;
	    }

	    public long getItemId(int position) {
	        return 0;
	    }

	    // create a new ImageView for each item referenced by the Adapter
	    public View getView(int position, View convertView, ViewGroup parent) {
	        ImageView imageView;
	        if (convertView == null) {  // if it's not recycled, initialize some attributes
	            imageView = new ImageView(mContext);
	            //imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
	            imageView.setLayoutParams(new GridView.LayoutParams(185, 185));
	            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
	            //imageView.setPadding(8, 8, 8, 8);
	            imageView.setPadding(24, 24, 24, 24);
	        } else {
	            imageView = (ImageView) convertView;
	        }

	        String fullpath = filepath + "/" +  fList[position];
	        Bitmap bitmap = BitmapFactory.decodeFile(fullpath);
	        imageView.setImageBitmap(bitmap);
	        
	        return imageView;
	    }
	}
}
