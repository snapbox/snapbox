package com.example.snapbox;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class ChooseEventFragment extends DialogFragment {
	List<Event> mSelectedEvents;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		mSelectedEvents = new ArrayList<Event>();
		
		final List<Event> events = Event.listAll(Event.class);
		
		String[] eventNames = new String[events.size()];
		for (int i = 0; i < events.size(); i++) {
			eventNames[i] = events.get(i).name;
		}
		
		boolean[] eventsToCheck = new boolean[events.size()];
		for (int i = 0; i < events.size(); i++) {
			eventsToCheck[i] = false;
		}
		
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		builder.setTitle("Choose an event").setMultiChoiceItems(eventNames, eventsToCheck,
				new DialogInterface.OnMultiChoiceClickListener() {
//					public void onClick(DialogInterface dialog, int which) {
//						mSelectedEvent = events.get(which);
//					}

					@Override
					public void onClick(DialogInterface dialog, int which,
							boolean isChecked) {
						if (isChecked) {
							if (!mSelectedEvents.contains(events.get(which))) {
								mSelectedEvents.add(events.get(which));
							}
						}
						else {if (mSelectedEvents.contains(events.get(which))) {
							mSelectedEvents.remove(events.get(which));
						}
						}
					}
				})
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		               @Override
		               public void onClick(DialogInterface dialog, int id) {
		                   // User clicked OK, so save the mSelectedItems results somewhere
		                   // or return them to the component that opened the dialog

		            	    EventDialogFragmentListener activity = (EventDialogFragmentListener) getActivity();
		            	    activity.onReturnValue(mSelectedEvents);
		               }
		           })
		        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		               @Override
		               public void onClick(DialogInterface dialog, int id) {
		            	    EventDialogFragmentListener activity = (EventDialogFragmentListener) getActivity();
		            	    activity.onReturnValue(null);
		               }
		           });
		// Create the AlertDialog object and return it
		return builder.create();
	}
}
