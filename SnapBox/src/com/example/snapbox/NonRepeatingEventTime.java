package com.example.snapbox;

import android.content.Context;

import com.orm.SugarRecord;

public class NonRepeatingEventTime extends SugarRecord<NonRepeatingEventTime> {
	int startYear;
	int startMonth;
	int startDay;
	int startHour;
	int startMinute;
	int endYear;
	int endMonth;
	int endDay;
	int endHour;
	int endMinute;
	Event event;

	public NonRepeatingEventTime(Context context) {
		super(context);
	}

	public NonRepeatingEventTime(Context context, int startYear,
			int startMonth, int startDay, int startHour, int startMinute,
			int endYear, int endMonth, int endDay, int endHour,
			int endMinute, Event event) {
		super(context);
		this.startYear = startYear;
		this.startMonth = startMonth;
		this.startDay = startDay;
		this.startHour = startHour;
		this.startMinute = startMinute;
		this.endYear = endYear;
		this.endMonth = endMonth;
		this.endDay = endDay;
		this.endHour = endHour;
		this.endMinute = endMinute;
		this.event = event;
	}

}