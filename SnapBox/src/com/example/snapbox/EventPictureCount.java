package com.example.snapbox;

import android.content.Context;

import com.orm.SugarRecord;

public class EventPictureCount extends SugarRecord<EventPictureCount> {
	Event event;
	int count;

	public EventPictureCount(Context context) {
		super(context);
	}

	public EventPictureCount(Context context, Event event, int count) {
		super(context);
		this.event = event;
		this.count = count;
	}

}
